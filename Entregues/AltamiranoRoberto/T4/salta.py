#!/usr/bin/python
#-*- coding: utf-8-*-
# isx47262285
# ROBERTO ALTAMIRANO MARTINEZ
# descripcion: script que hace un salto a paginas web random 
# apuntes:
#   se tiene que activar el sticky bit y tambien se ha de copiar en el 
#   directorio '/var/www/cgi-bin/' y luego ejecutar en el navegador 
#		url:	localhost/cgi-bin/salta.py
#   accesslog:  retorna 302 213  ( redireccion, sucess)
#########################################################################
import random
import os, sys

write = sys.stdout.write

#lista de direcciones url al azar
llista_URL = ['www.sport.es','www.fedoraproject.org','www.escoladeltreball.org','www.maxima.fm']

# utilizamos el metodo random para cojer una pag de la lista predefinida
URL = random.sample(llista_URL,1)

# Capçalera location
write('Content-Type: text/html; charset=UTF-8\r\n')
write('Location: http://%s \r\n' %(URL[0]))
write('\r\n')


# comprovar el codigo de retorno !

# $ tail -f /var/log/httpd/access_log
'''
ozilla/5.0 (X11; Fedora; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0"
127.0.0.1 - - [19/Mar/2019:09:13:53 +0100] "GET /cgi-bin/salta.py HTTP/1.1" 302 205 "-" "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0"
127.0.0.1 - - [19/Mar/2019:09:15:19 +0100] "GET /cgi-bin/salta.py HTTP/1.1" 302 213 "-" "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0"

'''
