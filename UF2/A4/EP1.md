Adaptació del gestor de continguts
==================================

MP9UF2A4EP1

Exercici pràctic (EP1)

Característiques de l’exercici
------------------------------

Exercici pràctic.

### Tipus d’exercici

Aquest exercici servirà per mesurar les habilitats en utilitzar MarkDown
i Git.

### Enunciat

El dia de realització de l’exercici el seu enunciat estarà disponible en
format XHTML en el [repositori local](_EP1.html) de fitxers.

### Criteri de qualificació

L’exercici aporta el 60% de la nota del resultat d’aprenentatge associat
a l’activitat.
