Configurar l’entorn de treball
==============================

MP9UF2A2T1

Organització i processament de documents

Com ho fa Docker?
-----------------

Docker ens proporciona un bon exemple de com gestionar documentació. Per
una banda, els documents original s’escriuen amb MarkDown, tal com pots
comprovar en el [repositori de
GitHub](https://github.com/docker/docker/tree/master/docs/reference/commandline)
on resideixen. Per altra banda aquests documents, transformats a HTML,
es publiquen en la [web de
Docker](https://docs.docker.com/engine/reference/commandline/cli/). En
les pràctiques a realitzar veurem com això es pot fer.

Us de GitLab com a repositori remot
-----------------------------------

Els diversos fitxers a usar en aquesta activitat han de residir en un
repositori de Git, del qual farem una còpia remota a GitLab. Les ordres
de Git que et caldrà usar són aquestes:

-   `git clone`
-   `git status`
-   `git log`
-   `git push`
-   `git pull`
-   `git remote`

Enllaços recomanats
-------------------

-   [GitLab](https://gitlab.com/)
-   [Xuleta de Git](aux/git-cheat-sheet.pdf) (local)
-   Moltes altres [xuletes](https://goo.gl/wgcGYR) de Git

Pràctiques
----------

-   Crea un compte a GitLab.
-   Crea una clau amb `ssh-keygen` i usa-la per configurar l’accés
    a GitLab.
-   Crea un directori per aquest projecte i puja’l a GitLab (ho pots fer
    en ordre invers si vols: crea un nou repositori a GitLab i clona’l).
    Edita apropiadament el fitxer `README.md`.
-   Còpia dos o tres documents del [repositori de
    Docker](https://github.com/docker/docker/tree/master/docs/reference/commandline)
    en aquest directori i posa’ls sota el control de Git.

